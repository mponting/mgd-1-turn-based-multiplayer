// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameStateBase.h"
#include "GS.generated.h"
/**
 * 
 */
UCLASS()
class TURNBASED2_API AGS : public AGameStateBase
{
	GENERATED_BODY()

public:
	AGS();

	UFUNCTION(Server, Reliable, WithValidation)
		void RotateTurn();

	UFUNCTION(Server, Reliable, WithValidation)
		void TakeTurn(int PlayerID);

	UFUNCTION(Server, Reliable, WithValidation)
		void GenerateTurnOrder();

	//Variables//
	TArray<TQueue<int>> TurnOrder;

};
